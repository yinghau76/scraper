package scraper

import (
	"io"
	"net/http"

	"github.com/andybalholm/cascadia"
	"golang.org/x/net/html"
	"golang.org/x/text/transform"
)

// Get retrieves a HTML page and return the parsed document
func Get(url string, t transform.Transformer) (doc *html.Node, err error) {
	resp, err := http.Get(url)
	if err != nil {
		return
	}
	defer resp.Body.Close()

	var r io.Reader = resp.Body
	if t != nil {
		r = transform.NewReader(resp.Body, t)
	}

	doc, err = html.Parse(r)
	return
}

// GetNodeText extracts text inside a HTML node and its decendant nodes
func ScrapeText(n *html.Node) string {
	var t string
	for c := n.FirstChild; c != nil; c = c.NextSibling {
		if c.Type == html.TextNode {
			t += c.Data
		} else {
			t += ScrapeText(c)
		}
	}
	return t
}

func ScrapeAllText(root *html.Node, sel cascadia.Selector) []string {
	var items []string
	for _, n := range sel.MatchAll(root) {
		items = append(items, ScrapeText(n))
	}
	return items
}

func ScrapeTable(root *html.Node, rowSel cascadia.Selector, colSel cascadia.Selector) [][]string {
	var rows [][]string
	for _, row := range rowSel.MatchAll(root) {
		rows = append(rows, ScrapeAllText(row, colSel))
	}
	return rows
}
